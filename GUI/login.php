<?php

include_once '../DAO/login_register.php';

if ($_POST) {
    $email = $_REQUEST['email'];
    $pasword = $_REQUEST['password'];
    $user = login($email, $pasword);
    
    
    if ($user) {
        $count = cantidad($user[0]);
        session_start();
        $_SESSION['user'] = $user;
        if ($user[7] === 'Administrador') {
            header('Location:categories.php');
        } else if ($user[7] === 'Usuario') {
            if ($count[0] > 0) {

                header('Location:newsCover.php');
            } else {
                header('Location:newSources.php');
            }
        }
    } else {
        echo '<p class="alert alert-success agileits text-center" role="alert">Usuario no encontrado! </p>';
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>

<body>
    <div class="container">
        <div class="container">
            <header class="bg-white ">
                <nav class="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center ">
                    <img src="/Img/noticias.svg" width="230" height="80" class="d-inline-block align-top" alt=""
                        loading="lazy">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="button-nav">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul class="navbar-nav text-secondary">
                            <li class="nav-item" id="line2">
                                <a class="nav-link bg-secondary text-white" href="#" tabindex="-1"
                                    aria-disabled="true"><img src="/Img/icons8_user_32px_2.png" width="20" height="20"
                                        class="d-inline-block align-top" alt="" loading="lazy"> Login</a>
                            </li>

                        </ul>
                    </div>
                </nav>
            </header>
        </div>

        <!-- Header -->

        <div class="container pt-1">
            <div class="jumbotron bg-white text-secondary ">
                <h4 class="display-6 text-center">User Login</h4>
                <hr class="my-4 bg-secondary w-25">

            </div>
        </div>

        <!-- tittle -->
        <div class="container">
            <main class="bg-white pt-4 pt-0" style="margin-top: -7rem;">
                <form class="text-center" method="POST">
                    <div class="form-row d-flex flex-column align-items-center pt-5">

                        <div class="form-group col-md-3 ">
                            <input type="email" class="form-control " id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Email Address" name="email">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="password" class="form-control" id="exampleInputPassword1"
                                placeholder="Password" name="password">


                        </div>
                        <div>
                            <!-- <div class="alert alert-light " role="alert" id="alert">Usuario no identificado </div> -->

                        </div>


                        <br>
                        <hr class=" bg-secondary w-50 ">
                        <label class="form-check-label text-secondary mb-3 ">If you don't have an account <a
                                href="Register.php" style="color:#c95555; ">Singup Here</a></label>


                        <div class="form-group ">
                            <button type="submit " class="btn btn-secondary btn-sm m-1 mb-4"
                                id="button-login">Login</button>
                        </div>
                    </div>
                </form>
            </main>


        </div>


        <div class="container pt-5 ">
            <footer class="bg-white  pt-2">
                <ul class="nav justify-content-center ">
                    <li class="nav-item active ">
                        <a class="nav-link text-secondary " href="">My Cover </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-secondary ">|</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-secondary " href="">About</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-secondary ">|</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-secondary " href="# ">Help</a>
                    </li>
                </ul>
                <ul class="nav justify-content-center ">
                    <a class="nav-link text-secondary " href="# " tabindex="-1 " aria-disabled="true ">© My News Cover
                    </a>
                </ul>
            </footer>
        </div>
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
</script>

</html>