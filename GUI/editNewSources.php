<?php
include_once '..//DAO/sources.php';

session_start();

$user = $_SESSION['user'];
if (!$user) {
    header('Location: login.php');
}

$Categories = category();

if (isset($_GET['id'])) {
    $id =  $_GET['id'];
    $newSource = newSourceID($id);
}
if ($_POST) {
    $id =  $_GET['id'];
    edit($_REQUEST, $id);
}



?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">

    <title>Add Category</title>
</head>

<body>
    <div class="container">
        <header class="bg-white ">
            <nav class="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center ">
                <img src="/Img/noticias.svg" width="230" height="80" class="d-inline-block align-top" alt=""
                    loading="lazy">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="button-nav">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav text-secondary">
                        <li class="nav-item dropdown bg-secondary">
                            <a class="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#"
                                role="button" aria-expanded="false"><img src="/Img/icons8_user_32px_2.png" width="20"
                                    height="20" class="d-inline-block align-top" alt="" loading="lazy"> Logout</a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link text-secondary" href="#" tabindex="-1" aria-disabled="true">News
                                        Cover</a></li>
                                <li> <a class="nav-link text-secondary" href="#" tabindex="-1" aria-disabled="true">New
                                        Resources</a></li>
                                <li> <a class="nav-link bg-secondary text-white" href="logout.php" tabindex="-1"
                                        aria-disabled="true">Logout <img src="/Img/icons8_exit_32px.png" width="20"
                                            height="20" class="d-inline-block align-top" alt="" loading="lazy"></a></li>


                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>


    <div class="container pt-1">
        <div class="jumbotron bg-white text-secondary ">
            <h4 class="display-6 text-center">New Sources</h4>
            <hr class="my-4 bg-secondary w-25">

        </div>
    </div>

    <!-- tittle -->

    <div class="container ">
        <main class="bg-white pt-4 pt-0" style="margin-top: -2rem;">
            <form class="text-center" id="form" method="POST">
                <div class="form-row justify-content-center">
                </div>

                <div class="form-row d-flex flex-column align-items-center">

                    <div class="form-group col-md-3 ">
                        <input type="hidden" class="form-control" placeholder="id" aria-describedby="inputGroupPrepend"
                            required name="idUser" value="<?php echo $user[0]; ?>">
                        <input type="hidden" class="form-control" placeholder="id" aria-describedby="inputGroupPrepend"
                            required name="idNewSource" value="<?php echo $newSource[0]; ?>">
                        <input type="text" class="form-control" placeholder="Name" aria-describedby="inputGroupPrepend"
                            required name="Name" value='<?php echo $newSource[1]; ?>'>

                    </div>
                    <div class="form-group col-md-3">
                        <input type="text" class="form-control" placeholder="Url RSS"
                            aria-describedby="inputGroupPrepend" required name="url"
                            value="<?php echo $newSource[2]; ?>">
                    </div>
                    <div class="form-group col-md-3">
                        <select class="form-control" name="category">
                            <?php
                            foreach ($Categories as $category) :
                                if ($category[0] === $newSource[4]) {
                            ?>
                            <option value=<?php echo $category[0] ?> selected disabled hidden><?php echo $category[1] ?>
                            </option>
                            <?php
                                }
                                ?>
                            <option value=<?php echo $category[0] ?>><?php echo $category[1] ?></option>
                            <?php

                            endforeach;
                            ?>
                        </select>
                    </div>


                    <hr class=" bg-secondary w-50">
                    <button type="submit" class="btn btn-secondary btn-sm m-1 mb-4" id="button-Register">Save</button>
                </div>
            </form>
        </main>
    </div>

    <!-- footer -->
    <div class="container pt-5 mt-3">
        <footer class="bg-white  pt-5 mt-5">
            <ul class="nav justify-content-center ">
                <li class="nav-item active ">
                    <a class="nav-link text-secondary " href="">My Cover </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary ">|</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary " href="">About</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary ">|</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary " href="# ">Help</a>
                </li>
            </ul>
            <ul class="nav justify-content-center ">
                <a class="nav-link text-secondary " href="# " tabindex="-1 " aria-disabled="true ">© My News Cover
                </a>
            </ul>
        </footer>
    </div>
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
</script>

</html>