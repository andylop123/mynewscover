<?php
include_once '..//DAO/newsFuntions.php';
$categories = category();



session_start();
$user = $_SESSION['user'];
$count = cantidad($user[0]);
$news = news(0,$user[0]);

if (!$user && $user[7] != 'Usuario') {
    header('Location: login.php');
}

if ($count[0] <= 0) {
    header('Location: newSources.php');
}
if (isset($_GET['id'])) {
    $id =  $_GET['id'];
    $news = news($id,$user[0]);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>News Cover</title>
</head>

<body>
    <div class="container">
        <header class="bg-white ">
            <nav class="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center ">
                <img src="/Img/noticias.svg" width="230" height="80" class="d-inline-block align-top" alt=""
                    loading="lazy">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="button-nav">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav text-secondary">
                        <li class="nav-item dropdown bg-secondary">
                            <a class="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#"
                                role="button" aria-expanded="false"><img src="/Img/icons8_user_32px_2.png" width="20"
                                    height="20" class="d-inline-block align-top" alt="" loading="lazy">
                                <?php echo $user[1]; ?></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link text-secondary" href="#" tabindex="-1" aria-disabled="true">News
                                        Cover</a></li>
                                <li> <a class="nav-link text-secondary" href="newSources.php" tabindex="-1"
                                        aria-disabled="true">New Sources</a></li>
                                <li> <a class="nav-link bg-secondary text-white" href="logout.php" tabindex="-1"
                                        aria-disabled="true">Logout <img src="/Img/icons8_exit_32px.png" width="20"
                                            height="20" class="d-inline-block align-top" alt="" loading="lazy"></a></li>


                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>

    <!-- Header -->

    <div class="container pt-1">
        <div class="jumbotron bg-white text-secondary ">
            <h4 class="display-6 text-center">Your Unique News Cover</h4>
            <hr class="my-4 bg-secondary w-25">

        </div>
    </div>

    <!-- tittle -->
    <div class="container  mb-5 pb-5">
        <div class="card-group">

            <a class="btn btn-secondary btn-sm px-3" id="button-login" href="newsCover.php?id=0">Portada</a>
            <?php
            foreach ($categories as $category) { ?>
            <div class="card">
                <a class="btn btn-secondary btn-sm " id="button-login"
                    href="newsCover.php?id=<?php echo $category[0]; ?>"><?php echo $category[1]; ?></a>
            </div>
            <?php } ?>
        </div>
    </div>




    <div class="container ">
        <main class="bg-white  ">
            <div class="row">

                <?php
                foreach ($news as $item) { ?>


                <div class="col mb-4 col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text"><?php echo fecha($item[4]); ?></p>
                            <h5 class="card-title"><?php echo $item[2]; ?></h5>
                            <p class="card-text"><?php echo $item[2]; ?></p>
                            <a class="btn btn-secondary btn-sm " id="button-login" href="<?php echo $item[3]; ?>">See
                                News</a>
                        </div>
                    </div>
                </div>

                <?php } ?>


            </div>
        </main>
    </div>



    <div class="container pt-5 ">
        <footer class="bg-white  pt-2">
            <ul class="nav justify-content-center ">
                <li class="nav-item active ">
                    <a class="nav-link text-secondary " href="">My Cover </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary ">|</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary " href="">About</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary ">|</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary " href="# ">Help</a>
                </li>
            </ul>
            <ul class="nav justify-content-center ">
                <a class="nav-link text-secondary " href="# " tabindex="-1 " aria-disabled="true ">© My News Cover
                </a>
            </ul>
        </footer>
    </div>
</body>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
</script>

</html>