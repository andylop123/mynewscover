<?php

include_once '../DAO/Categories.php';
if ($_POST) {
    $id = $_REQUEST['id'];
    edit($_REQUEST, $id);
}
if (isset($_GET['id'])) {
    $id =  $_GET['id'];
    $category = category($id);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">

    <title>Add Category</title>
</head>

<body>
    <div class="container">
        <header class="bg-white ">
            <nav class="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center ">
                <img src="/Img/noticias.svg" width="230" height="80" class="d-inline-block align-top" alt=""
                    loading="lazy">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="button-nav">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav text-secondary">
                        <li class="nav-item dropdown bg-secondary">
                            <a class="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#"
                                role="button" aria-expanded="false"><img src="/Img/icons8_user_32px_2.png" width="20"
                                    height="20" class="d-inline-block align-top" alt="" loading="lazy"> Logout</a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link text-secondary" href="#" tabindex="-1" aria-disabled="true">News
                                        Cover</a></li>
                                <li> <a class="nav-link text-secondary" href="#" tabindex="-1" aria-disabled="true">New
                                        Resources</a></li>
                                <li> <a class="nav-link bg-secondary text-white" href="#" tabindex="-1"
                                        aria-disabled="true">Logout <img src="/Img/icons8_exit_32px.png" width="20"
                                            height="20" class="d-inline-block align-top" alt="" loading="lazy"></a></li>


                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>

    <!-- Header -->

    <div class="container pt-1">
        <div class="jumbotron bg-white text-secondary ">
            <h4 class="display-6 text-center">Categories</h4>
            <hr class="my-4 bg-secondary w-25">

        </div>
    </div>

    <!-- tittle -->

    <div class="container ">
        <main class="bg-white pt-4 pt-0" style="margin-top: -2rem;">
            <form class="text-center" id="form" method="POST">
                <div class="form-row justify-content-center">
                </div>

                <div class="form-row d-flex flex-column align-items-center">

                    <div class="form-group col-md-3 ">
                        <!-- <label for="inputAddress">Address</label> -->
                    </div>
                    <div class="form-group col-md-3">
                        <input type="hidden" class="form-control" placeholder="Category"
                            aria-describedby="inputGroupPrepend" required name="id" value="<?php echo $category[0]; ?>">
                        <input type="text" class="form-control" placeholder="Category"
                            aria-describedby="inputGroupPrepend" required name="Category"
                            value="<?php echo $category[1]; ?>">
                    </div>


                    <hr class=" bg-secondary w-50">
                    <button type="submit" class="btn btn-secondary btn-sm m-1 mb-4" id="button-Register">Save</button>
                </div>
            </form>
        </main>
    </div>

    <!-- footer -->
    <div class="container pt-5 mt-3">
        <footer class="bg-white  pt-5 mt-5">
            <ul class="nav justify-content-center ">
                <li class="nav-item active ">
                    <a class="nav-link text-secondary " href="">My Cover </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary ">|</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary " href="">About</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary ">|</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-secondary " href="# ">Help</a>
                </li>
            </ul>
            <ul class="nav justify-content-center ">
                <a class="nav-link text-secondary " href="# " tabindex="-1 " aria-disabled="true ">© My News Cover
                </a>
            </ul>
        </footer>
    </div>
</body>

</html>