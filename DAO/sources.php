<?php

/**
 * Undocumented function
 * este metodo obtine la conexion la BD
 * @return void la conexion
 */
function getConection()
{
    $connection = mysqli_connect('localhost:3306', 'root', '', 'mynewscover');
    return $connection;
};

// SELECT COUNT(*) from newsources
/**
 * Undocumented function
 *Este metodo devuelve una lista
 * @return void devuelve la lista de las
 */
function category()
{
    $con = getConection();
    $datos = "SELECT * FROM category";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_all($resultado);
    //$data=mysqli_fetch_assoc($resultado);
    mysqli_close($con);
    return $data;
};

/**
 * Undocumented function
 *este metodo devuelve las new sources de un usuario
 * @param [type] $id parametro por el cual buscar el id
 * @return void me devuelve el new source
 */
function newSources($id)
{
    $con = getConection();
    $datos = "SELECT n.id,n.nombre, url, c.nombre FROM newsources n INNER JOIN category c on n.id_category =c.id where  n.id_user = '$id'";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_all($resultado);
    //$data=mysqli_fetch_assoc($resultado);
    mysqli_close($con);
    return $data;
};

/**
 * Undocumented function
 *este metodo devuelve un new source por id
 * @param [type] $id parametro por el cual buscar el id
 * @return void me devuelve el new source
 */
function newSourceID($id)
{
    $con = getConection();
    $datos = "select * from newsources where id = '$id'";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_row($resultado);
    return $data;
};

/**
 * Undocumented function
 *este metodo devuelve un new source por id
 * @param [type] $id parametro por el cual buscar el id
 * @return void me devuelve el new source
 */
function newSourceToID($id)
{
    $con = getConection();
    $datos = "SELECT n.id,n.nombre, url, c.nombre FROM newsources n INNER JOIN category c on n.id_category =c.id where  n.id_user = '$id'";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_all($resultado);
    return $data;
};

/**
 * Undocumented function
 *Este metodo inserta un nuevo recurso a la base de datos 
 * @param [type] $data datos a insertar
 */
function insert($data)
{

    $con = getConection();

    $Name = $data['Name'];
    $URL = $data['url'];
    $IdCategory = $data['category'];
    $IdUser = $data['idUser'];



    $datos = "INSERT INTO newsources( nombre, url, id_user, id_category) VALUES ('$Name','$URL','$IdUser','$IdCategory')";
    $resultado = mysqli_query($con, $datos);

    if (!$resultado > 0) {
        echo '<p class="alert alert-success agileits text-center" role="alert">Error when registering new Source! </p>';
    } else {

        echo '<p class="alert alert-success agileits text-center" role="alert"> New Source succesfelly registered! </p>';
        header('location:newSources.php');
    }
    mysqli_close($con);
};
/**
 * Undocumented function
 *Este metodo edita un new source
 * @param [type] $data los datos que se van a editar
 * @param [type] $id id al cual se va a editar
 */
function edit($data, $id)
{

    $con = getConection();


    $Name = $data['Name'];
    $URL = $data['url'];
    $IdCategory = $data['category'];
    $IdUser = $data['idUser'];



    $datos = " UPDATE newsources SET nombre='$Name' ,url = '$URL', id_user = '$IdUser', id_category ='$IdCategory' WHERE id =$id";
    $resultado = mysqli_query($con, $datos);

    if (!$resultado > 0) {
        echo '<p class="alert alert-success agileits text-center" role="alert">Error when editing new Source! </p>';
    } else {

        echo '<p class="alert alert-success agileits text-center" role="alert"> New Source succesfelly Edited! </p>';
        header('location:newSources.php');
    }
    mysqli_close($con);
};

/**
 * Undocumented function
 *Este metodo elimina un recurso por id 
 * @param [type] $id id para eliminar el recurso
 */
function delete($id)
{
    $con = getConection();
    $datos = "DELETE FROM newsources WHERE id = '$id'";
    $resultado = mysqli_query($con, $datos);
    mysqli_close($con);
};
