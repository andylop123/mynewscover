<?php

/**
 * Undocumented function
 * este metodo obtine la conexion la BD
 * @return void la conexion
 */
function getConection()
{
    $connection = mysqli_connect('localhost:3306', 'root', '', 'mynewscover');
    return $connection;
};


/**
 * Undocumented function
 *Este metodo me devuelve las categorias
 * @return void devuelve las categorias en un arreglo
 */
function category()
{
    $con = getConection();
    $datos = "SELECT * FROM category";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_all($resultado);
    //$data=mysqli_fetch_assoc($resultado);
    mysqli_close($con);
    return $data;
};


/**
 * Undocumented function
 *esta metodo devuelve una lista
 */
function newSources()
{
    $con = getConection();
    $datos = "SELECT * FROM newsources n INNER JOIN category c on n.id_category =c.id ";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_all($resultado);
    //$data=mysqli_fetch_assoc($resultado);
    mysqli_close($con);
    return $data;
};

/**
 *este metodo devuelve una lista
 * @param [type] $id id por el cual se va a buscar la lista
 */
function news($id,$idUser)
{
    $con = getConection();
    if ($id == 0) {
        $datos = "SELECT * FROM news where id_user='$idUser'";
    } else {

        $datos = "SELECT * FROM news where  id_category = '$id' and id_user='$idUser'";
    }
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_all($resultado);
    //$data=mysqli_fetch_assoc($resultado);
    mysqli_close($con);
    return $data;
};

/**
 * Undocumented function
 *Este metodo inserta las noticias en la BD
 * @param [type] $data datos de new sources
 * @param [type] $rss datos del rss de noticias
 * @return void si lo guado o no lo guardo
 */
function insert($data, $rss)
{

    $con = getConection();

    $id_newSource = $data[0];
    $id_user = $data[3];
    $Id_category = $data[4];
    $link = $rss->link;  //extrae el link
    $title = $rss->title;  //extrae el titulo
    $date = $rss->pubDate;  //extrae la fecha
    $description = $rss->description;  //extrae la descripcion



    $datos = "INSERT INTO news(title, short_description, permanlink, fecha, id_newSource, id_user, id_category) VALUES ('$title','$description','$link','$date','$id_newSource','$id_user','$Id_category')";
    $resultado = mysqli_query($con, $datos);
    mysqli_close($con);
};


/**
 * Undocumented function
 *Este metodo elimina todas las noticias 
 * @return void devuelve un bool dependiendo de la accion
 */
function delete()
{
    $con = getConection();
    $datos = "DELETE FROM news";
    $resultado = mysqli_query($con, $datos);
    mysqli_close($con);
};
/**
 * Undocumented function
 *Este metodo me devuelve la cantidad de una lista
 * @return void la canntidad
 */
function cantidad($id)
{
    $con = getConection();
    $datos = "SELECT COUNT(*) from newsources where id_user='$id'";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_row($resultado);
    return $data;
}


/**
 * Undocumented function
 *Este metodo convierte un string a un formato de fecha
 * @param [type] $date string a convertir
 * @return void la fecha convertida
 */
function fecha($date)
{

    $dt = new DateTime($date);
    return $dt->format('d/m/Y   H:i A'); // imprime 29/03/2018
}
