<?php

/**
 * Este metodo es la connexion a la base de datos.
 */
function getConection()
{
    $connection = mysqli_connect('localhost:3306', 'root', '', 'mynewscover');
    return $connection;
};

/**
 * Inserta una categoria en la Base de datos 
 * @data datos a ingresar
 */
function insert($data)
{

    $con = getConection();

    $Name = $data['Category'];


    $datos = "INSERT INTO category( nombre) VALUES ('$Name')";
    $resultado = mysqli_query($con, $datos);

    if (!$resultado > 0) {
        echo '<p class="alert alert-success agileits text-center" role="alert">Error when registering category! </p>';
    } else {

        echo '<p class="alert alert-success agileits text-center" role="alert"> Category succesfelly registered! </p>';
        header('location:categories.php');
    }
    mysqli_close($con);
};

/**
 * etida un categoria por medio del id
 * @data categoria a editar
 * @id id a categoria a editar
 */
function edit($data, $id)
{

    $con = getConection();

    $Name = $data['Category'];


    $datos = " UPDATE category SET nombre='$Name' WHERE id =$id";
    $resultado = mysqli_query($con, $datos);

    if (!$resultado > 0) {
        echo '<p class="alert alert-success agileits text-center" role="alert">Error when editing category! </p>';
    } else {

        echo '<p class="alert alert-success agileits text-center" role="alert"> Category succesfelly Edited! </p>';
        header('location:categories.php');
    }
    mysqli_close($con);
};


/**
 * 
 * me devuelve la lista de las categorias
 */
function datos()
{
    $con = getConection();
    $datos = "SELECT * FROM category";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_all($resultado);
    //$data=mysqli_fetch_assoc($resultado);
    mysqli_close($con);
    return $data;
};


/**
 * Elimina una categoria por id
 * @id cual categoria voy a eliminar 
 */
function delete($id)
{
    $con = getConection();
    $datos = "DELETE FROM category WHERE id = '$id'";
    $resultado = mysqli_query($con, $datos);
    mysqli_close($con);
};




/**
 * Carga una categoria por id
 * @id a buscar 
 */
function category($id)
{
    $con = getConection();
    $datos = "select * from category where id ='$id'";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_row($resultado);
    return $data;
};
