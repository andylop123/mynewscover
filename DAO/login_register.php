
<?php
/**
 * Este metodo es la connexion a la base de datos.
 */
function getConection()
{
    $connection = mysqli_connect('localhost:3306', 'root', '', 'mynewscover');
    return $connection;
};



/**
 * 
 * me devuelve la lista de las roles
 */
function datos()
{
    $con = getConection();
    $datos = "SELECT * FROM rol";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_all($resultado);
    //$data=mysqli_fetch_assoc($resultado);
    mysqli_close($con);
    return $data;
};


/**
 * Inserta un usuario en la Base de datos 
 * @data datos a ingresar
 */
function insert($data)
{

    $con = getConection();

    $firstName = $data['FirtsName'];
    $lastName = $data['LastName'];
    $email = $data['Username'];
    $password = $data['password'];
    $idRol = $data['Rol'];

    $datos = "INSERT INTO users(nombre, apellido, email,password,id_rol) VALUES ('$firstName','$lastName','$email','$password','$idRol')";
    $resultado = mysqli_query($con, $datos);

    if (!$resultado > 0) {
        echo '<p class="alert alert-success agileits text-center" role="alert">Error al registrarse! </p>';
    } else {

        echo '<p class="alert alert-success agileits text-center" role="alert">  Usuario registrado correctamente! </p>';
        header('location:login.php');
    }
    mysqli_close($con);
};

/**
 * Este metodo autentica el usuario
 * @email correo de usuario
 * @password contraseña de usuario
 */
function login($email, $password)
{
    $con = getConection();
    $datos = "select * from users u INNER JOIN rol r on u.id_rol = r.id where  email='$email' and password  = '$password'";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_row($resultado);
    return $data;
};

/**
 * Undocumented function
 *Este metodo me devuelve la cantidad de una lista
 * @return void la canntidad
 */
function cantidad($id)
{
    $con = getConection();
    $datos = "SELECT COUNT(*) from newsources where id_user='$id'";
    $resultado = mysqli_query($con, $datos);
    $data = mysqli_fetch_row($resultado);
    return $data;
}
