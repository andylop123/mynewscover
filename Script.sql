CREATE DATABASE mynewscover;
CREATE TABLE rol(
  id int AUTO_INCREMENT PRIMARY key,
  nombre text NOT null
);
INSERT INTO
  rol(nombre)
VALUES
  ('Usuario');
INSERT INTO
  rol(nombre)
VALUES
  ('Administrador');
CREATE TABLE users(
    id int AUTO_INCREMENT PRIMARY key,
    nombre text NOT null,
    apellido text NOT null,
    email text NOT null,
     password text NOT null,
    id_rol int not null,
    FOREIGN KEY(id_rol) REFERENCES rol(id)
  );

  CREATE table category(
id int AUTO_INCREMENT PRIMARY KEY,
    nombre text not null
);


CREATE TABLE news(
    id int AUTO_INCREMENT PRIMARY key,
    title text NOT null,
    short_description text NOT null,
    permanlink text NOT null,
     fecha date NOT null,
    id_newSource int not null,
    id_user int not null,
    id_category int not null,
    FOREIGN KEY(id_newSource) REFERENCES newsources(id),
    FOREIGN KEY(id_user) REFERENCES users(id),
    FOREIGN KEY(id_category) REFERENCES category(id)

    
  );